<?php 

error_reporting(E_ALL);
ini_set('max_execution_time', 6); //300 seconds = 5 minutes
set_time_limit(60);


/*
Plugin Name: Costco Fans OCR
Description: Costco Fans general site functions.
Version:     2.2
Author:      Avanfeel
Author URI:  https://avanfeel.com
License:     GPL2
 
"Costco Fan Blogs" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
"Costco Fan Blogs" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with "Costco Fan Blogs". If not, see {License URI}.
*/

// use thiagoalessio\TesseractOCR\TesseractOCR;
/**
 * Include the file with the PluginUpdater class.
 */
// require_once 'wp-gitlab-updater/plugin-updater.php';
  
// /**
//  * Init the plugin updater with the plugin base name.
//  */
// new Moenus\GitLabUpdater\PluginUpdater( [
//     'slug' => './wp-content/plugins/costco', 
//     'plugin_base_name' => 'costco/index.php', 
//     'access_token' => '1ZodARtUAFXociXgSt34', 
//     'gitlab_url' => 'https://gitlab.com/danishali',
//     'repo' => 'koibhinaam',
// ] );

class CostcoFan{

	// const PLUGIN_PATH = 'costco-fans-1';
	const PLUGIN_PATH = 'costco-fans';

	static function init(){
		add_shortcode( 'costco-upload-form', array( 'CostcoFan', 'uploadForm' ));
	}

	static function newPost(
		$post_title,
		$post_content
	){

		// Create post object
		$my_post = array(
		  'post_title'    => wp_strip_all_tags( $post_title ),
		  'post_content'  => ( $post_content ),
		  'post_status'   => 'draft',
		  // 'post_status'   => 'publish',
		  'post_author'   => 1
		);
		 
		// Insert the post into the database
		return wp_insert_post( $my_post );
	}


	static function responseError($err){
		return [
			'success' => false,
			'error' => $err,
		];
	}


	static function responseSuccess($res){
		return [
			'success' => true,
			'result' => $res,
		];
	}


	static function attachMedia(){}



	static function reviewOCR($review, $post_id){

		$post_images = [];

		foreach ($review as $attachment_id => $ocr) {

			$ocr_handle = strtolower(preg_replace("/[^a-zA-Z0-9]+/", " ", $ocr));
			$ocr_line  = preg_replace("/[\n\r]/", " ", $ocr);
			$ocr_line  = str_replace(',', ' ', $ocr_line);

			$ocr_line = str_ireplace('exp.', 'exp', $ocr_line);
			$ocr_line = str_ireplace('exp', 'EXPIRY DATE:', $ocr_line);

			$ocr = str_ireplace('exp.', 'exp', $ocr);
			$ocr = str_ireplace('exp', 'EXPIRY DATE:', $ocr);

			$args = array( 
                'ID'           => $attachment_id, 
                'post_title'   => $ocr_handle, 
                'post_excerpt' => $ocr,
                'post_content' => $ocr
            );

			update_post_meta( $attachment_id, '_wp_attachment_image_alt', $ocr );

    		wp_update_post( $args );

    		$attachment_image = wp_get_attachment_image($attachment_id, array(1000,750), false, ['alt' => $ocr_line, 'caption' => $ocr_line]);
    		if ($attachment_image) $post_images[] = '[caption id="" align="alignnone" width="1200"]'.$attachment_image. $ocr_line.'[/caption]';


		}


		// $post_images = array_reverse($post_images);


		$post = get_post($post_id); 

		array_unshift($post_images, $post->post_content);
		
		$post_with_new_images = array(
            'ID' => $post_id,
            'post_content' => implode("\n", $post_images)
        );

		remove_all_actions('save_post');
		$wp_update_post = wp_update_post( $post_with_new_images );


		
		$path = plugin_dir_path(__FILE__) . 'queue/';
		foreach (new DirectoryIterator($path) as $fileInfo) {
		    if($fileInfo->isDot()) continue;
		    $file =  $path.$fileInfo->getFilename();
		    unlink($file);
		}


	}



	static function setQueue($data){

		$total_images = count($data['images']['tmp_name']);

		$uploads_dir = dirname(__FILE__)."/queue/";

		$count=0;

		
		foreach ($data["images"]["error"] as $key => $error) {
		    if ($error == UPLOAD_ERR_OK) {
		        $tmp_name = $data["images"]["tmp_name"][$key];
		        // basename() may prevent filesystem traversal attacks;
		        // further validation/sanitation of the filename may be appropriate
		        $name = basename($data["images"]["name"][$key]);
		        move_uploaded_file($tmp_name, "$uploads_dir/$name");
		    	$count++;    
		    }
		    
		}

		return $count;

	}



	static function countQueue(){
		$fi = new FilesystemIterator(plugin_dir_path(__FILE__) . 'queue/', FilesystemIterator::SKIP_DOTS);
		return iterator_count($fi);	
	}


	static function updatePost(){

		// remove_action('post_updated', 'wp_save_post_revision', 10);
		// remove_action('save_post', 'my_function');
		// remove_action( 'post_updated', 'wp_save_post_revision' );



		$new_post = array(
            'ID' => 64714,
            'post_content' => 'xxx',
            'post_type' => 'draft',
        );

		$wp_update_post = wp_update_post( $new_post );

		return (CostcoFan::responseSuccess($wp_update_post));
	}


	static function processFiles($post_id, $page=1){

		remove_action('post_updated', 'wp_save_post_revision', 10);
		remove_action('save_post', 'my_function');
		remove_action( 'post_updated', 'wp_save_post_revision' );

		$absPath = plugin_dir_url(__FILE__) . 'queue/';

		$path = plugin_dir_path(__FILE__) . 'queue/';
		
		$blacklist = array('somedir','somefile.php');

		foreach (new DirectoryIterator($path) as $fileInfo) {
		    if($fileInfo->isDot()) continue;
		    $file =  $path.$fileInfo->getFilename();
		    $data[] = $file;
		}


		usort(
			$data,

			function($a, $b){
				return strcmp($a, $b);
			}

		);

		if (floor($page) < 1) $page=1;
		$total_images = count($data);
		$page_size = 2;


		$offset = ($page - 1) * $page_size;

		$total_pages = $total_images/$page_size;

		if ($total_images/$page_size > floor($total_images/$page_size)) $total_pages = floor($total_images/$page_size)+1;
		
		if ($page_size > $total_images) $page_size = $total_images;

		$post_images = [];
		$errors = [];


		$review = [];


		for ($i=$offset; $i < $offset+$page_size; $i+=2) { 

			$tmp_names = [$data[$i], $data[$i+1]];

			$image_label = $tmp_names[1];
			$image_photo = $tmp_names[0];

			if (!$image_label || !$image_photo) continue;

			// OCR lines

			$time_a = time();
			$ocr_lines = CostcoFan::ocr(
				$absPath.basename($image_label)
			);

			$ocr_lines = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $ocr_lines);	

			$time_b = time();

			$ocr_lines_str = implode('', $ocr_lines);
			$ocr_lines_str = str_replace("\n", '', $ocr_lines_str);

			$ocr_lines_str = substr($ocr_lines_str, 0, 100);

			$merged = CostcoFan::merge(
				$image_photo,
				$image_label,
				$ocr_lines
			);


			// addWatermark($merged);

			$merged_path = plugins_url()."/".CostcoFan::PLUGIN_PATH."/$merged";


			$attachment_id = crb_insert_attachment_from_url($merged_path, $post_id, $ocr_lines_str);


			$review[] = [
				'image'		=> plugins_url()."/".CostcoFan::PLUGIN_PATH."/queue/".basename($image_label),
				'ocr_lines' => $ocr_lines,
				'id' => $attachment_id,
				'time'=> ($time_b-$time_a)
			];

			$attachment_image = wp_get_attachment_image($attachment_id, 'large', false, ['alt' => $ocr_lines_str, 'caption' => $ocr_lines_str]);

			if (!$attachment_image) $errors[] = "$attachment_id / $merged_path";

			if ($attachment_image) $post_images[] = $attachment_image;




		}


		$remaining = $total_images - ($offset + ($page_size * $page));

		$p = round(($total_images-$remaining) / $total_images * 100);

		$next_page = $page+1;

		if ($i-2 > count($data)){

			$next_page=0;
			// remove all images
			for ($i=0; $i < $offset+$page_size; $i+=2) { 
				// unlink($data[$i]);
				// unlink($data[$i+1]);
			}

			

		}


		$ret = [
			'page'				=> floor($page),
			'total_pages'		=> $total_pages,
			'total_images'		=> $total_images,
			'next_page'			=> $next_page,
			'progress'			=> $p,
			'wp_update_post'	=> $wp_update_post,
			'post_images'		=> $post_images2,
			'errors'			=> $errors,
			'review'			=> $review
		];		

		return $ret;

	}



	static function ocr($img_path){

		$file_name = basename( $img_path );

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, site_url().'/wp-json/costco-fans-ocr?image='.$file_name);

        
        // curl_setopt($ch, CURLOPT_URL, 'https://api.ocr.space/parse/imageurl');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, "apikey=OCRK6398898A&url=$img_path");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, "apikey=d469dfaa2c88957&url=$img_path");
        $output = curl_exec($ch);
        curl_close($ch);


        
        $json = json_decode($output);

        if (!$json->success) throw new Exception("ocr: $img_path (filename $file_name): $output", 1);
        
        // $ParsedText = $res->ParsedText;

        return $json->result;

	}






	static function merge($img_path_1, $img_path_2, $ocr_lines = []){

		$date_dir = date('Y-m-d');

		$base_dir = dirname(__FILE__)."/uploads/$date_dir/";
		$base_dir_rel = "uploads/$date_dir/";

		if (!file_exists($base_dir)) mkdir($base_dir, 0777, true);

		$img_photo = imagecreatefromjpeg($img_path_1); // PHOTO
		$img_tag = imagecreatefromjpeg($img_path_2); // TAG

		list($width_photo, $height_photo) = getimagesize($img_path_1);
		list($width, $height) = getimagesize($img_path_2);

		$ratio_photo = $width_photo/$height_photo;
		$ratio_tag = $width/$height;

		$newwidth = 333;
		$newheight = $newwidth/$ratio_tag;


		$width_photo2 = 1000;
		$height_photo2 = $width_photo2/$ratio_photo;

		$img_tag_res = imagecreatetruecolor($newwidth, $newheight);
		$img_photo_res = imagecreatetruecolor($width_photo2, $height_photo2);

		
		imagecopyresized($img_tag_res, $img_tag, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagecopyresized($img_photo_res, $img_photo, 0, 0, 0, 0, $width_photo2, $height_photo2, $width_photo, $height_photo);

		imagealphablending($img_photo_res, false);
		imagesavealpha($img_photo_res, true);

		imagecopymerge($img_photo_res, $img_tag_res, 0, 0, 0, 0, $newwidth, $newheight, 100); //have to play with these numbers for it to work for you, etc.

		// ob_start();
 	// 	imagejpeg($img_photo);
 	// 	$baseimg = ob_get_clean();

		// echo move_uploaded_file($img_path_1, dirname(__FILE__).'/wilchooo2.png');

		$ocr_text = implode('-', $ocr_lines);

		$ocr_text = preg_replace("/[^a-zA-Z0-9]+/", " ", $ocr_text);

		$ocr_text = str_replace('  ', ' ', $ocr_text);
		$ocr_text = str_replace(' ', '-', $ocr_text);
		$ocr_text = strtolower($ocr_text);

		$ocr_text_short = substr($ocr_text, 0, 100);

		$rand = time();
		$target_file_name = $rand.'-'.$ocr_text_short.".jpg";
		$target_file = $base_dir."$target_file_name";
		$target_file_rel = $base_dir_rel."$target_file_name";

		imagejpeg($img_photo_res, $target_file);

		imagedestroy($img_photo_res);
		imagedestroy($img_tag);


		/**
	     * @Start Adding watermark
	     */
	    $wm = imagecreatefrompng(realpath(getcwd().'/wp-content/plugins/'.CostcoFan::PLUGIN_PATH.'/watermark-1b.png'));
	    imagefilter($wm, IMG_FILTER_COLORIZE, 0,0,0,127*.2); // the fourth parameter is alpha

	    $im = imagecreatefromjpeg($target_file);
	    $marge_right = 10;
	    $marge_bottom = 10;
	    $sx = imagesx($wm);
	    $sy = imagesy($wm);


	    imagecopy($im, $wm, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($wm), imagesy($wm));
	    imagejpeg($im, $target_file);
	    /**
	     * @END Adding watermark
	     */

	    

		return $target_file_rel;

	}





	// Register Custom Post Type
	static function register_custom_post_type() {

		$labels = array(
			'name'                  => _x( 'Sportsbooks', 'Post Type General Name', 'sportsbooks' ),
			'singular_name'         => _x( 'Sportsbook', 'Post Type Singular Name', 'sportsbooks' ),
			'menu_name'             => __( 'Sportsbooks', 'sportsbooks' ),
			'name_admin_bar'        => __( 'Sportsbook', 'sportsbooks' ),
			'archives'              => __( 'Sportsbooks Archives', 'sportsbooks' ),
			'attributes'            => __( 'Sportsbook Attributes', 'sportsbooks' ),
			'parent_item_colon'     => __( 'Parent Item:', 'sportsbooks' ),
			'all_items'             => __( 'All Sportsbooks', 'sportsbooks' ),
			'add_new_item'          => __( 'Add New Sportsbook', 'sportsbooks' ),
			'add_new'               => __( 'Add New', 'sportsbooks' ),
			'new_item'              => __( 'New Sportsbook', 'sportsbooks' ),
			'edit_item'             => __( 'Edit Sportsbook', 'sportsbooks' ),
			'update_item'           => __( 'Update Sportsbook', 'sportsbooks' ),
			'view_item'             => __( 'View Sportsbook', 'sportsbooks' ),
			'view_items'            => __( 'View Sportsbooks', 'sportsbooks' ),
			'search_items'          => __( 'Search Sportsbook', 'sportsbooks' ),
			'not_found'             => __( 'Not found', 'sportsbooks' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'sportsbooks' ),
			'featured_image'        => __( 'Featured Image', 'sportsbooks' ),
			'set_featured_image'    => __( 'Set featured image', 'sportsbooks' ),
			'remove_featured_image' => __( 'Remove featured image', 'sportsbooks' ),
			'use_featured_image'    => __( 'Use as featured image', 'sportsbooks' ),
			'insert_into_item'      => __( 'Insert into Sportsbook', 'sportsbooks' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'sportsbooks' ),
			'items_list'            => __( 'Sportsbooks list', 'sportsbooks' ),
			'items_list_navigation' => __( 'Sportsbooks list navigation', 'sportsbooks' ),
			'filter_items_list'     => __( 'Filter Sportsbooks list', 'sportsbooks' ),
		);

		$rewrite = array(
			'slug'                  => 'sportsbook',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);

		$args = array(
			'label'                 => __( 'Sportsbook', 'sportsbooks' ),
			'description'           => __( 'Post Type Description', 'sportsbooks' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-book',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'post',
			'show_in_rest'          => true,
			'rest_base'             => 'sportsbooks',
		);

		register_post_type( 'sportsbook', $args );	
		

	}


	static function uploadForm(){

		ob_start();

		$step = floor($_GET['step']);
		$step_name = $step+1;	?>


		<style type="text/css">

			.ocr-steps {
				display: flex;
				justify-content: center;
				padding: 20px;
			}

			.ocr-steps-nav .current{
				font-weight: bold;
			}


		</style>


		<ul class="ocr-steps-nav">
			<li class="<?php echo ($step == 0 ? 'current' : '') ?>">1. Select images</li>
			<li class="<?php echo ($step == 1 ? 'current' : '') ?>">2. Select post title</li>
			<li class="<?php echo ($step == 2 ? 'current' : '') ?>">3. Start OCR extraction</li>
			<li class="<?php echo ($step == 3 ? 'current' : '') ?>">4. Review OCR</li>
		</ul>


		<div class="ocr-steps">

<?		switch ($step) {

			case 1:	?>






				<form action="?step=2" method="POST">
					
					<label>
						<input placeholder="Post title" type="" name="title">
					</label>

					<input type="submit" name="" value="Next">

				</form>
					

<?

				break;
			
			case 2:

			$post_title = $_POST['title'];

			$content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

			$post_id = CostcoFan::newPost(
				$post_title,
				$content
			);


			$queue_size = CostcoFan::setQueue($_FILES);	?>

			<button onclick="window.open('?step=3&post=<?php echo $post_id; ?>&queue_size=<?php echo $queue_size; ?>', '_self')">START IMAGE PROCESS (<?php echo CostcoFan::countQueue(); ?>)</button>

<?


			break;

			case 3:


				$queue_size = $_GET['queue_size'];
				$post_id = $_GET['post'];
				$link = get_the_permalink($post_id);	?>


			<style type="text/css">

				.ocr-steps{
					text-align: center;
				}

				.costcocr-review__error{
				    align-items: center;
				    justify-content: center;
				    padding-top: 10%!important;
				}

				.progress .label{
					text-align: center;
					margin-bottom: 20px;
				}

				.progress .label{
					text-align: center;
				}

				.costcocr-review__retry {
					margin-right: 10px;
				}

				.costcocr-review{
					max-width: 100%!important;
					text-align: center;
				}

				.costcocr-review__form ul{
					/*display: flex;*/
					/*flex-direction: row;*/
				}
				
				.costcocr-review__form li{
				    width: calc(25% - 2px);
				    float: left;
				    list-style-type: none;
				    padding-right: 20px;
				    margin: 0;
				    padding: 0;
				    border: 1px solid black;
				}

				.costcocr-review__form li:nth-child(4n+1){
				    clear: both;
				}

				.costcocr-review__form textarea{
					width: calc(100% - 10px);
					border: 0;
					font-size: 12px;
					padding: 5px;
					font-family: sans-serif;
				}

				.costcocr-review--loading{
					opacity: .5
				}

				.costcocr-review--changed{
					outline: 5px solid red;
				}

			</style>



			<div class="costcocr-review">

			<div class="costcocr-review__progress ui progress">
			  
			  <div class="bar">
			    <div class="progress"></div>
			  </div>
			  <div class="label">Processing files... <span class="p"></span> • <span class="ocr__error-count">0</span> errors</div>

			</div>

			<div class="costcocr-review__buttons" style="display: none;">
				<!-- <button value="retry">Retry</button> -->
				<button onclick="submitRevisions(this);" value="submit">Submit revisions</button>
			</div>

			<form class="costcocr-review__form">
			</form>

		</div>


			<script type="text/javascript">

				errorCount=0;

				function renderForReview(review, page){

					const $review = jQuery('.costcocr-review__form');

					let $review_ul = $review.find('ul');

					if ($review_ul.length == 0) $review_ul = jQuery('<ul>');

					$review.removeClass('costcocr-review--loading');
					
					review.forEach(
						(o,i) => {

							if (!o.id) return;

							const $image = jQuery('<img>');
							const $textArea = jQuery(`<textarea name="review[${o.id}]" rows="10">`);
							const $checkbox = jQuery(`<input type='checkbox' style='position: absolute;'>`);
							const $li = jQuery('<li>');

							$image.attr('src', o.image);
							$textArea.val(o.ocr_lines);

							$textArea.on(
								'change',
								() => {
									$li.addClass('costcocr-review--changed');
								}
							);

							$li.append($image);
							$li.append($textArea);
							// $li.append($checkbox);

							$review_ul.append($li);
						}
					);

					const $button = jQuery('<button>');
					const $retry = jQuery('<button>');

					$button.text('NEXT >>');
					$retry.text('RETRY');

					$retry.addClass('costcocr-review__retry');
					$button.addClass('costcocr-review__next');

					$retry.prop('type', 'button');
					$button.prop('type', 'button');

					$button.click( (e) => { e.preventDefault(); $review.addClass('costcocr-review--loading'); $button.attr('disabled', true); processImages(page+1); return false; });
					$retry.click( (e) => { e.preventDefault(); $review.addClass('costcocr-review--loading'); $button.attr('disabled', true); processImages(page); return false; });

					$retry.hide();

					$review.append($review_ul);
					// $review.append($retry);
					// $review.append($button);

					// $button.click();

					

				}


				function showRetryButton(){
					jQuery('.costcocr-review__retry').show();
				}

				function enableButtons(){
					
					jQuery('.costcocr-review').removeClass('costcocr-review--loading');

					jQuery('.costcocr-review__retry').attr('disabled', false);
					jQuery('.costcocr-review__next').attr('disabled', false);
				}


				function processFinished(){

					jQuery('.costcocr-review__progress').hide();
					jQuery('.costcocr-review__buttons').show();

				}


				function processError(error){

					errorCount++;

					jQuery('.costcocr-review__error').remove();
					const $review = jQuery('.costcocr-review__form');
					let $review_ul = $review.find('ul');
					if ($review_ul.length == 0) $review_ul = jQuery('<ul>');
					const $li = jQuery('<li>');
					$li.addClass('costcocr-review__error');
					$li.html(`<h4>Error!</h4><p>${error}<p>Retrying #${errorCount}...`);
					$review_ul.append($li);

					jQuery('.ocr__error-count').html(errorCount);

					// $review.append($retry);
					// $review.append($button);

					// $button.click();

					


				}


				
				function processNext() {
					processImages(page++);
				}


				function processImages(page, review = false){

					if (!page) page = 1;

					const formData = jQuery('.costcocr-review__form').serialize();

					const data = `post=<?php echo $post_id; ?>&page=${page}`;
					
					console.log('data: ', data);

					jQuery.ajax({
						url: `<?php echo site_url(); ?>/wp-json/costco-fans`,
						type: 'POST',
						data: data,
						dataType: 'JSON',
						success: r => {

							console.log('r', r);
							jQuery('.costcocr-review__error').remove();


							if (!r.success) {
								// alert('A critical error has occured! '+r.error);
								processError(r.error);
								processImages(page);
								showRetryButton();
								enableButtons();
								return;
							}
							
							
							let t = parseFloat('<?php echo $queue_size ?>');
							let s = parseFloat(r);
							let p = r.result.progress;

							let p_page = r.result.page;
							let p_total_pages = r.result.total_pages;
							

							if(p_total_pages < p_page){

								document.title = `DONE!`;

								alert('done!');
								
								// window.open('<?php echo $link; ?>', '_self');

								processFinished();

							}else{

								jQuery('.p').text(`${p_page} of ${p_total_pages}`);

								document.title = `${p_page}/${p_total_pages}`;

								if (r.result.errors.length > 0){
									// alert('AN OCR ERROR HAS OCCURED!');
									// processError(r);
									// showRetryButton();
									// enableButtons();
								}

								renderForReview(r.result.review, page);
								processImages(page+1);
							}

						},
						error: r => {
							console.log(r);
							// alert('A SERVER ERROR HAS OCCURED!');

							if(r.responseJSON && r.responseJSON.message){
								processError(r.responseJSON.message);
							}else{
								processError(r);	
							}
							
							processImages(page);
							showRetryButton();
							enableButtons();
						}
					});

				}



				function submitRevisions(button){

					jQuery(button).prop('disabled', true);
					jQuery(button).text('Please wait...');

					// const formData = jQuery('.costcocr-review__form').serialize();


					let data = `post=<?php echo $post_id; ?>&`;

					jQuery('.costcocr-review__form textarea').each(
						(i, textarea) => {
							console.log(textarea);

							let $textarea = jQuery(textarea);

							let name = $textarea.prop('name');
							let val = encodeURI($textarea.val());

							data += `${name}=${val}&`;

						}
					);



					
			

					jQuery.ajax({
						url: `<?php echo site_url(); ?>/wp-json/costco-fans-revisions`,
						type: 'POST',
						data: data,
						dataType: 'JSON',
						success: r => {


							window.open('<?php echo $link; ?>', '_self');
							console.log('r', r);



						},
						error: r => {
							console.log(r);
							alert('A SERVER ERROR HAS OCCURED!');
							showRetryButton();
							enableButtons();
						}
					});

				}



				processImages();



			</script>

<?


				break;


				default:	?>



				<form method='post' action='' enctype="multipart/form-data">
					   <input type="file" id='files' name="files[]" multiple> <input type="button" id="submit" value='Next'>
					</form>


					<script type="text/javascript">
										


					$('#submit').click(function(){

						$('#submit').prop('disabled', 'disabled');
						$('#submit').val('Please wait...');

					   var form_data = new FormData();

					   // Read selected files
					   var totalfiles = document.getElementById('files').files.length;
					   for (var index = 0; index < totalfiles; index++) {
					      form_data.append("files[]", document.getElementById('files').files[index]);
					   }

					   // AJAX request
					   $.ajax({
					     url: '<?php echo site_url() ?>/wp-json/costco-fans-images',
					     type: 'post',
					     data: form_data,
					     dataType: 'json',
					     contentType: false,
					     processData: false,
					     success: function (response) {

					     	console.log('success', response);

					       for(var index = 0; index < response.length; index++) {
					         var src = response[index];

					         // Add img element in <div id='preview'>
					         // $('#preview').append('<img src="'+src+'" width="200px;" height="200px">');
					       }

					       window.open('?step=1', '_self');

					     },
					     error: function (error){
					     	console.log('error', error);
					     }
					   });

					});


									</script>


				

<?

					break;
			
			
		}	?>

	</div>

<?php

		return ob_get_clean();


	}

}

add_action( 'plugins_loaded', array( 'CostcoFan', 'init' ));




/**
* Downloads an image from the specified URL and attaches it to a post as a post thumbnail.
*
* @param string $file    The URL of the image to download.
* @param int    $post_id The post ID the post thumbnail is to be associated with.
* @param string $desc    Optional. Description of the image.
* @return string|WP_Error Attachment ID, WP_Error object otherwise.
*/
function Generate_Featured_Image( $file, $post_id, $desc ){
    // Set variables for storage, fix file filename for query strings.
    preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches );
    if ( ! $matches ) {
         return new WP_Error( 'image_sideload_failed', __( 'Invalid image URL' ) );
    }

    $file_array = array();
    $file_array['name'] = basename( $matches[0] );

    // Download file to temp location.
    $file_array['tmp_name'] = download_url( $file );

    // If error storing temporarily, return the error.
    if ( is_wp_error( $file_array['tmp_name'] ) ) {
        return $file_array['tmp_name'];
    }

    // Do the validation and storage stuff.
    $id = media_handle_sideload( $file_array, $post_id, $desc );

    // If error storing permanently, unlink.
    if ( is_wp_error( $id ) ) {
        @unlink( $file_array['tmp_name'] );
        return $id;
    }
    return set_post_thumbnail( $post_id, $id );

}



function crb_insert_attachment_from_url($url, $parent_post_id = null, $ocr_lines_str) {

	if( !class_exists( 'WP_Http' ) )
		include_once( ABSPATH . WPINC . '/class-http.php' );

	$http = new WP_Http();
	$response = $http->request( $url );
	if( $response['response']['code'] != 200 ) {
		return false;
	}

	$upload = wp_upload_bits( basename($url), null, $response['body'] );
	if( !empty( $upload['error'] ) ) {
		return false;
	}

	$file_path = $upload['file'];
	$file_name = basename( $file_path );
	$file_type = wp_check_filetype( $file_name, null );
	$attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
	$wp_upload_dir = wp_upload_dir();

	$post_info = array(
		'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
		'post_mime_type' => $file_type['type'],
		'post_title'     => $attachment_title,
		// 'post_content'   => $ocr_lines_str,
		'post_content'   => '',
		'post_status'    => 'inherit',
	);

	// Create the attachment
	// $attach_id = wp_insert_attachment( $post_info, $file_path, $parent_post_id );
	$attach_id = wp_insert_attachment( $post_info, $file_path, 0, true );

	if (is_wp_error($attach_id)) {
		print_r([$post_info, $file_path]);
		throw new Exception($attach_id->get_error_messages(), 1);
	}

	// Include image.php
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	// Define attachment metadata
	$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

	// Assign metadata to attachment
	wp_update_attachment_metadata( $attach_id,  $attach_data );

	return $attach_id;

}



add_action( 'rest_api_init', function ( $server ) {
    

    $server->register_route( 'costco-fans', '/costco-fans', array(
        'methods'  => 'POST',
        'callback' => function () {
        	$post = $_POST['post'];
        	$page = $_POST['page'];
        	$review = $_POST['review'];
        	

        	if ($review){
        		CostcoFan::reviewOCR(
        			$review,
        			$post
        		);
        	}


        	try {
        		$processFilesRes = CostcoFan::processFiles($post, $page);	
        	} catch (Exception $e) {

        		return (CostcoFan::responseError($e->getMessage()));

        	}

        	return (CostcoFan::responseSuccess($processFilesRes));
        	

        },
    ) );




    $server->register_route( 'costco-fans-revisions', '/costco-fans-revisions', array(
        'methods'  => 'POST',
        'callback' => function () {
        	$post = $_POST['post'];
        	$review = $_POST['review'];

        	// print_r($review);
        	// die;
        	
        	$res = CostcoFan::reviewOCR(
        			$review,
        			$post
        		);

        	return (CostcoFan::responseSuccess($res));
        	

        },
    ) );



    function ocr2($image) {
  $text = shell_exec("/usr/bin/tesseract --psm 4 $image stdout");
  return (is_null($text)) ? "" : $text;
}


	
	$server->register_route( 'costco-fans-images', '/costco-fans-images', array(
        'methods'  => 'POST',
        'callback' => function () {

        	// Count total files
	$countfiles = count($_FILES['files']['name']);

	// Upload directory
	$upload_location = "queue/";

	// To store uploaded files path
	$files_arr = array();

	// Loop all files
	for($index = 0;$index < $countfiles;$index++){

	   if(isset($_FILES['files']['name'][$index]) && $_FILES['files']['name'][$index] != ''){
	      // File name
	      $filename = $_FILES['files']['name'][$index];

	      // Get extension
	      $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

	      // Valid image extension
	      $valid_ext = array("png","jpeg","jpg");

	      // Check extension
	      if(in_array($ext, $valid_ext)){

	         // File path
	         // $path = $upload_location.$filename;

	      	$path = dirname(__FILE__)."/queue/$filename";
	         // Upload file
	         if(move_uploaded_file($_FILES['files']['tmp_name'][$index],$path)){
	            $files_arr[] = $path;
	         }
	      }
	   }
	}

	return json_encode($files_arr);
        	

        },
    ) );




    $server->register_route( 'costco-fans-ocr', '/costco-fans-ocr', array(
        'methods'  => 'GET',
        'callback' => function () {

        	$image = $_GET['image'];
        	$image_name = basename($image);

        	$base_dir = dirname(__FILE__)."/queue/";

        	// IMG_FILTER_GRAYSCALE
			$img_tag = imagecreatefromjpeg($base_dir.$image_name); // TAG
			if (!imagefilter($img_tag, IMG_FILTER_GRAYSCALE)) die('IMAGE Grayscale conversion');

			unlink(getcwd().'/wp-content/plugins/'.CostcoFan::PLUGIN_PATH.'/queue/'.$image_name);
			
			if (!imagejpeg($img_tag, $base_dir.$image_name)) die($base_dir.$image_name);

   
        	$res = ocr2(realpath(getcwd().'/wp-content/plugins/'.CostcoFan::PLUGIN_PATH.'/queue/'.$image_name));
        	

        	return (CostcoFan::responseSuccess($res));
        	

        },
    ) );


   
} );


